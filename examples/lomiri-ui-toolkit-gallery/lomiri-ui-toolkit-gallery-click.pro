TEMPLATE = subdirs

SUBDIRS += po

TARGET = lomiri-ui-toolkit-gallery

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  lomiri-ui-toolkit-gallery.apparmor \
               lomiri-ui-toolkit-gallery.png

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               lomiri-ui-toolkit-gallery.desktop

#specify where the qml/js files are installed to
qml_files.path = /
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is
#automatically created in the build directory
desktop_file.path = /
desktop_file.files = lomiri-ui-toolkit-gallery.desktop
desktop_file.CONFIG += no_check_exist

PROJECT_VERSION = $$(PROJECT_VERSION)
isEmpty(PROJECT_VERSION) {
    #set default version if not set
    PROJECT_VERSION = 0.6
}

QMAKE_SUBSTITUTES +=manifest.json.in
manifest_file.path = /
manifest_file.files = manifest.json


INSTALLS+=config_files qml_files desktop_file manifest_file
